const express = require('express');
const bodyParser = require('body-parser');
const Routes = require('./server/routes');
const ConfigMiddleware = require('./server/middleware');


const routes = new Routes();
const configMiddleware = new ConfigMiddleware();
const app = express()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/home', (request, response) => {
    response.send('Application is Running')
})
configMiddleware.initPreMiddleware();
routes.init(app)
app.use('/demo', Routes)
app.listen(3030, () => console.log("listening on port 3030"));
