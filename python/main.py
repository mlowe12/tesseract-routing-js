import sys
import uuid
import logging
import connexion
from PIL import Image
from flask import Flask, jsonify, make_response, send_file
from connexion.resolver import RestyResolver

from model import crop
from model import definitions as defn



def image_service_handler(bound_type):
    doc = connexion.request.files['document']
    try:
        c = crop.ImageSegmentation(Image.open(doc),defn.SIGNAL_INTERPOLATION_VALUE)
        img = c.fit(bound_type)
    except Exception as err:
        logging.INFO('image segmentation class error encountered: %{0}'.format(err))
    return send_file(img,as_attachment=True,attachment_filename='cropped.png')


if __name__ == "__main__":
    app = connexion.App(__name__, specification_dir='.')
    app.add_api('app.yaml',resolver=RestyResolver('api'))
    app.run(port=8088)