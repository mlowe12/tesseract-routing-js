import io
import json
from PIL import Image
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

class ImageSegmentation(object):
    
    def __init__(self, image, rolling_window):
        self.image = image
        self.flattened_signal = np.array(self.image.convert(mode='L'))
        self.height, self.width = self.flattened_signal.shape
        self.vertical_axis_mean = None
        self.output_image = None
        self.rolling_window = rolling_window

    def calculate_bound(self, data, boundary_type):
        if boundary_type.lower() == 'min':
            asymp = data.describe()['min']
            bool_val = data == asymp
        elif boundary_type.lower() == 'max':
            asymp = data.describe()['max']
            bool_val = data == asymp        
        else:
            return -1
        target_val = data[bool_val].index.to_list()
        last_idx = len(target_val)-1

        return target_val[last_idx]

    def fit(self, image_family):
        self.vertical_axis_mean = self.flattened_signal.mean(axis=1)
        rolling = pd.Series(self.vertical_axis_mean)\
            .rolling(self.rolling_window)\
            .mean()\
            .fillna(np.average(self.vertical_axis_mean)-.25*np.std(self.vertical_axis_mean))

        if image_family == 'bound':
            upper_bound = self.calculate_bound(rolling, 'min')
            lower_bound = self.calculate_bound(rolling, 'max')
            self.output_image = self.image.crop((0, lower_bound, self.width, upper_bound))
        elif image_family == 'continuous':
            lower_bound = self.calculate_bound(rolling, 'max')
            self.output_image = self.image.crop((0, lower_bound, self.width, self.height))
        else:
            return -1
        if self.output_image:
            return self.output_image
        else:
            return None

