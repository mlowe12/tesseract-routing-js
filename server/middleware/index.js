const PreMiddleware = require("./pre");
const PostMiddleware = require("./post");

class ConfigMiddleware {
    constructor(app) {
        this.app = app;
        this.preMiddleware = new PreMiddleware();
        this.postMiddleware = new PostMiddleware();
    }

    initPreMiddleware() {
        this.preMiddleware.init();
    }

    initPostMiddleware() {
        this.postMiddleware.init();
    }
}

module.exports = ConfigMiddleware;