const TestRoute = require('./test.route');
const TessRoute = require('./tess.route');

const testRoute = new TestRoute();
const tessRoute = new TessRoute();

class Routes {
    init(app) {
        testRoute.init(app);
        tessRoute.init(app);
    }
}

module.exports = Routes;