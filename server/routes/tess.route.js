const CropService = require('../services/crop.service');
const TesseractService = require('../services/tesseract.service');
const FileUpload = require('express-fileupload');
const fs = require('fs');
const path = require('path');
const uuid = require('uuid/v1');
const format = require('string-format');

class TessRoute {

    init(app) {
        const tesseractService = new TesseractService();
        const cropService =  new CropService();
        // delete this...
        const mockS3Bucket = "/media/michael/s3/ib-preprocessing"
        app.use(FileUpload());
        app.get('/api/v1/tessdata', async(request, response) => {
            const message = await tesseractService.extractTextFromDocument("~/Downloads/phi_free_1_1.png");
            return response.json({
                extractedText: message
            }); 
        });

        app.post('/api/v1/uploadDocument', async function(req, res) {
            console.log(req.files.input);
            const sid = String(uuid());
            const filepath = format('{0}/{1}.png', mockS3Bucket,sid);
            console.log(filepath);
            fs.writeFile(filepath,req.files.input.data);
            console.log("local s3 bucket: ", filepath)
            await cropService.processDocument(filepath);
            // const data = await tesseractService.extractTextFromDocument(format('/tmp/cropped-images/{0}.png', sid));
            return res.json({
                message: "request submitted",
                filepath: filepath
                // extractedText: data
            });
        });
    }

    postDocumentHandler(documentData,sequenceId) {
        return 
    }
}

module.exports = TessRoute;