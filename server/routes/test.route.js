const DummyService = require('../services/dummy.service');

class TestRoute {
    init(app) {
        const dummyService = new DummyService();
        app.get('/api/v1/poc', async(request, response) => {
            const message = await dummyService.sayHello();
            return response.json({
                message: message
            });
        });
        app.get('/api/v1/poc2', async(request, response) => {
            const message = await dummyService.dummyFunction();
            return response.json({
                message: message
            });
        });
    }
}

module.exports = TestRoute;