const BaseService = require('./base.service');
const process = require('child_process');
const path = require('path')
// const Opencv = require('opencv4nodejs');

class CropService extends BaseService {
    constructor() {
        super();
    }

    async processDocument(imageFilepath) {
        const { stdout } = process.spawn('python', ["./python/main.py", String(imageFilepath)]);
    }
}

module.exports = CropService;

// cs = new CropService('/home/michael/Downloads/phi_free_1_1.png');
// const t = cs.processDocument()