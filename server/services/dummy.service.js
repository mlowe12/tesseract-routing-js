const BaseService = require('./base.service');

class DummyService extends BaseService {
    constructor() {
        super();
    }

    async sayHello() {
        try {
            const content = "hello world!"
            return { content, processed: true }
        } catch(e) {
            console.error(e);
            return e;
        }
    }

    async dummyFunction() {
        try {
            const content = "little more excited hello world!"
            return { content, process: true }
        } catch(e) {
            console.error(e);
            return e
        }
    }
}

module.exports = DummyService;