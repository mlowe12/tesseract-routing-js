const os = require('os');
const tesseract = require('node-tesseract-ocr'); 
const dataframe = require('dataframe-js');
const dataforge = require('data-forge');
const BaseService = require('./base.service');


class TesseractService extends BaseService {
    constructor(preprocessArgs) {
        super();
        this.preprocessArgs = preprocessArgs;
        this.options = {
            psm: 6
          };
    }

    async extractTextFromDocument(filepath) {
        console.log("targeted filepath: ", this.filepath)
        const response = await tesseract.recognize(filepath, this.options);
        console.log("plain text: ", response);
        return response.split(os.EOL);

    }
}

module.exports = TesseractService;